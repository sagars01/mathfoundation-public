import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import {Http, Headers, URLSearchParams} from '@angular/http';
import { Response } from '@angular/http/src/static_response';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

const contenturl = environment.globalurl  + 'api/public/';
@Injectable()
export class StaticContentService {
  imageStaticPath: any = environment.globalurl + 'static/images';
  fullPath: any;

  constructor(private http: Http) {

  }

  getPageNotFoundImage(req) {
    this.fullPath = this.imageStaticPath + req + '.png';
    return this.fullPath;
  }
  getSubmitSuccessGif(req) {
    this.fullPath = this.imageStaticPath + req + '.gif';
    return this.fullPath;
  }
  getAboutPageContent(): Observable<any> {
    return this.http.get(contenturl + 'about').map(response => response.json());
  }
}

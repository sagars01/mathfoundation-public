import { Injectable } from '@angular/core';
import {Http, Headers, URLSearchParams} from '@angular/http';
import {environment} from '../../environments/environment';
import {ContactInterface , Country , RegisterUser, LoginInterface} from '../Interfaces/Global.interface';
import { Response } from '@angular/http/src/static_response';
// import { Observable } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const apiurl = environment.apiurl;
const globalurl = environment.globalurl;
const contenturl = environment.globalurl  + '/api/public/';
@Injectable()
export class GlobalService {
  constructor(private http: Http) { }
  // Urls
  registerUrl: any = apiurl + 'register'; // Registration
  contactDetailsUrl: any = apiurl + 'admincontact'; // Contact Page Information
  loginUrl: any = globalurl + 'login'; // For Login

  // Global error handler!!
  private handleError (error: Response) {
    return Observable.throw(error);
  }

  registerUser(userParams): Observable<RegisterUser[]> {
    return this.http.post(this.registerUrl , userParams).map((response: Response) => {
      return <RegisterUser[]>response.json();
    }).catch(this.handleError);
  }

  // Get all countries list
  getCountryList(): Observable<Country[]> {
    return this.http.get(apiurl + 'countrylist').map((response: Response) => {
      return <Country[]>response.json();
    }).catch(this.handleError);
  }

  // Login Service Upgrade
  loginUser(userParams): Observable<LoginInterface[]> {
    return this.http.post(this.loginUrl , userParams).map((response) => {
      return <LoginInterface[]>response.json();
    }).catch(this.handleError);
  }

  // Get Contact Page Details
  getContactPageDetails(): Observable<ContactInterface[]> {
    return this.http.get(apiurl + 'admincontact').map((response: Response) => {
      return <ContactInterface[]>response.json();
    }).catch(this.handleError);
    }
}

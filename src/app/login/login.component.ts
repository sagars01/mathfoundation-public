import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { GlobalService } from '../services-global/global.service';
import { Routes , RouterModule, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userparam: any = {};
  constructor(private gs: GlobalService , private route: Router, public toastr: ToastsManager , vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    localStorage.clear();
  }

  login(): any {
    // console.log(this.userparam);
    this.gs.loginUser(this.userparam).subscribe((response) => {
      this.toastr.success('Login Success' , 'You are awesome!');
      if (response['status'] === 200) {
        const username = response['name'].toLowerCase();
        localStorage.setItem('token', response['token']);
        this.route.navigate(['user' ,  username ]);
      }
    });
  }

}

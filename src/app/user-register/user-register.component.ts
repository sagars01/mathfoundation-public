import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services-global/global.service';


@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css'],
  providers : [GlobalService]
})
export class UserRegisterComponent implements OnInit {
  registerData: any = {
    // Adding data dynamically from the registration form
  };
  countries: any = [];
  showRegisterSuccess = false;
  showRegistrationForm = true;
  constructor(private gs: GlobalService) { }

  ngOnInit() {
    this._getCountryList();
  }

  _getCountryList() {
    this.gs.getCountryList().subscribe(response => {
      // console.log(response);
      this.countries = response;
    });
  }
  // Main Submit Function
  registerUser() {
    this.gs.registerUser(this.registerData).subscribe(response => {
      console.log(response);
      if ( response ['registered'] === true) {
        this.showRegisterSuccess = true;
        this.showRegistrationForm = false;
      }
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteloginComponent } from './sitelogin.component';

describe('SiteloginComponent', () => {
  let component: SiteloginComponent;
  let fixture: ComponentFixture<SiteloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sitelogin',
  templateUrl: './sitelogin.component.html',
  styleUrls: ['./sitelogin.component.css']
})
export class SiteloginComponent implements OnInit {

  constructor() { }
  userData: any;
  ngOnInit() {
    this.getUserInfofromStorage();
  }

  getUserInfofromStorage() {
    this.userData = JSON.parse(localStorage.getItem('userdata'));
  }

}

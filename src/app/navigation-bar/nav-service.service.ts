import { Injectable } from '@angular/core';

@Injectable()
export class NavbarService {

  constructor() { }
  navLinks = [
    {
      id : 1,
      name : 'Home',
      template : 'dummytemplate.html',
      routePath : '/'
    },
    {
      id : '2',
      name : 'About Us',
      template : 'dummytemplate.html',
      routePath : '/about'
    },
    {
      id : '3',
      name : 'Online Coaching',
      template : 'dummytemplate.html',
      routePath : '/coaching'
    },
    {
      id : '4',
      name : 'Sample Papers',
      template : 'dummytemplate.html',
      routePath : '/samplepapers'
    },
    {
      id : '5',
      name : 'Mock Test',
      template : 'dummytemplate.html',
      routePath : '/mocktest'
    },
    {
      id : '6',
      name : 'Contact Us',
      template : 'dummytemplate.html',
      routePath : '/contact'
    }
  ];

    getNavlinks() {
      return this.navLinks;
    }

}

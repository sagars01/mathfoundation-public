import { Component, OnInit , Input } from '@angular/core';
import { NavbarService } from './nav-service.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {
  // Data Passing via directives
  siteName: any = 'Math Foundation';

  constructor(private navService: NavbarService) {

  }
  brandPrimary = 'Math Foundation';
  navbarMenuItems: any = [];
  ngOnInit() {
    this.getNavigationLinks();
  }

  getNavigationLinks(): void {
      this.navbarMenuItems = this.navService.getNavlinks();
  }

  getNavigationLogo(): void {
  }

  manageNavStyling(): void {
  }

}

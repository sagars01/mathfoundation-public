import { Component, OnInit , Input } from '@angular/core';
import { StaticContentService } from '../../services-global/static-content.service';

@Component({
  selector: 'app-submit-success',
  templateUrl: './submit-success.component.html',
  styleUrls: ['./submit-success.component.css']
})
export class SubmitSuccessComponent implements OnInit {
  imageName = '/submit_success';
  imageLink = '';
  constructor( private sc: StaticContentService) { }

  @Input()
  visibility = false;

  ngOnInit() {
    this.getImageLink();
  }

  getImageLink() {
    this.imageLink = this.sc.getSubmitSuccessGif(this.imageName);
  }

}

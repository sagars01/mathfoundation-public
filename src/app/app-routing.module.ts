import { NgModule } from '@angular/core';
import { Routes , RouterModule } from '@angular/router';

// Component Imports

import { LoginComponent } from './login/login.component';
import { MainPageComponent } from './main-page/main-page.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { Error404Component } from './error404/error404.component';
import { ContactComponent } from './contact/contact.component';
import { UserhomeComponent } from './user.app/userhome/userhome.component';
import { FeedComponent } from './user.app/feed/feed.component';
import { AboutUsComponent } from './about-us/about-us/about-us.component';
import { OnlineCoachingComponent } from './online-coaching/online-coaching.component';
import { SamplePapersComponent } from './sample-papers/sample-papers.component';
import { MockTestComponent } from './mock-test/mock-test.component';

const routes: Routes = [
  {
    path : '',
    component : MainPageComponent
  },
   {
     path : 'home',
     component : MainPageComponent,
   },
   {
    path : 'about',
    component : AboutUsComponent,
  },
  {
    path : 'coaching',
    component : OnlineCoachingComponent,
  },
  {
    path : 'samplepapers',
    component : SamplePapersComponent,
  },
  {
    path : 'mocktest',
    component : MockTestComponent,
  },
  {
    path : 'login',
    component : LoginComponent
  },
  {
    path : 'register',
    component : UserRegisterComponent
  },
  {
    path : 'contact',
    component : ContactComponent
  },
  {
    path : 'user/:name',
    component : UserhomeComponent,
    children : [
      {
        path : '',
        redirectTo : 'feed',
        pathMatch : 'full'
      },
      {
        path : 'feed',
        component: FeedComponent
      }
    ]
  },
  {
    path : '404',
    component : Error404Component
  },
  {
    path : '**',
    redirectTo : '404'
  },

];

@NgModule({
  exports: [
    RouterModule
  ],
  imports : [RouterModule.forRoot(routes)],
  declarations: []
})

export class AppRoutingModule {
}

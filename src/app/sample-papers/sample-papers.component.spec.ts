import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SamplePapersComponent } from './sample-papers.component';

describe('SamplePapersComponent', () => {
  let component: SamplePapersComponent;
  let fixture: ComponentFixture<SamplePapersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SamplePapersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SamplePapersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

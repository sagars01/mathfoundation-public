import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import { ProblemDialogComponent } from '../problem-dialog/problem-dialog.component';

@Component({
  selector: 'app-problem-card',
  templateUrl: './problem-card.component.html',
  styleUrls: ['./problem-card.component.css']
})
export class ProblemCardComponent implements OnInit {

  constructor( public dialog: MatDialog) { }

  ngOnInit() {
  }

  openProblem() {
    const dialogRef = this.dialog.open(ProblemDialogComponent, {
      // height: '350px'
    });
  }
}

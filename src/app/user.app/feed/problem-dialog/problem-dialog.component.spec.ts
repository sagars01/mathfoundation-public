import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProblemDialogComponent } from './problem-dialog.component';

describe('ProblemDialogComponent', () => {
  let component: ProblemDialogComponent;
  let fixture: ComponentFixture<ProblemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProblemDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProblemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

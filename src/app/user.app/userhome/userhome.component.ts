import { Component, OnInit, OnDestroy , AfterViewInit , ElementRef } from '@angular/core';

@Component({
  selector: 'app-userhome',
  templateUrl: './userhome.component.html',
  styleUrls: ['./userhome.component.css']
})
export class UserhomeComponent implements OnInit , OnDestroy , AfterViewInit {

  constructor(private el: ElementRef) { }

  opened: any;
  ngOnInit() {
    this.opened = true;
  }

  ngOnDestroy() {
  }

  ngAfterViewInit() {

  }
}

import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import { UserService } from '../../services-user/user.service';

@Component({
  selector: 'app-ask-dialog',
  templateUrl: './ask-dialog.component.html',
  styleUrls: ['./ask-dialog.component.css']
})
export class AskDialogComponent implements OnInit {
  options: FormGroup;
  formUserInputData: any;
  hideFileType: any = true;

  constructor(fb: FormBuilder , private us: UserService) {
    this.options = fb.group({
      hideRequired: false,
      floatLabel: 'auto',
    });
    this.formUserInputData = {
      q_subject : '',
      q_statement : ''
    };
   }

  ngOnInit() {
  }

  postQuestion() {
    console.log(this.formUserInputData);
  }


}

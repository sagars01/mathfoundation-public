import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import { AskDialogComponent } from '../ask-dialog/ask-dialog.component';

@Component({
  selector: 'app-ask-btn',
  template: `<button mat-raised-button (click) = "openAskDialog()" color="warn">Ask Question</button>`,
  styles: []
})
export class AskBtnComponent implements OnInit {

  constructor(private askDialog: MatDialog) {

  }

  ngOnInit() {
  }
  openAskDialog() {
    const dialogRef = this.askDialog.open(AskDialogComponent, {
      height: '350px',
      width : '70%'
    });
  }
}

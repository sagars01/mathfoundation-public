import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule , ViewContainerRef } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule, Validators, FormBuilder } from '@angular/forms';
import {ToastModule} from 'ng2-toastr';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import 'hammerjs/hammer';

import { AppComponent } from './app.component';


// Service
import { NavbarService } from './navigation-bar/nav-service.service';
import { GlobalService } from './services-global/global.service';
import { StaticContentService } from './services-global/static-content.service';
import { UserService } from './user.app/services-user/user.service';

import { MainPageComponent } from './main-page/main-page.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { AppRoutingModule } from './/app-routing.module';
import { RouterModule } from '@angular/router/src/router_module';
import { OrderByPipe } from './pipes-global/order-by.pipe';
import { Error404Component } from './error404/error404.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { ContactComponent } from './contact/contact.component';
import { ContactFormComponent } from './shared/contact-form/contact-form.component';
import { ContactInfoComponent } from './shared/contact-info/contact-info.component';
import { LoginComponent } from './login/login.component';
import { SubmitSuccessComponent } from './shared/submit-success/submit-success.component';
import { UserhomeComponent } from './user.app/userhome/userhome.component';
import { FeedComponent } from './user.app/feed/feed.component';
import { SiteloginComponent } from './navigation-bar/sitelogin/sitelogin.component';
import { AboutUsComponent } from './about-us/about-us/about-us.component';
import { OnlineCoachingComponent } from './online-coaching/online-coaching.component';
import { SamplePapersComponent } from './sample-papers/sample-papers.component';
import { MockTestComponent } from './mock-test/mock-test.component';
import { ProblemCardComponent } from './user.app/feed/problem-card/problem-card.component';
import { MiniProfileComponent } from './user.app/feed/mini-profile/mini-profile.component';
import { CardFilterComponent } from './user.app/feed/card-filter/card-filter.component';
import { ProblemDialogComponent } from './user.app/feed/problem-dialog/problem-dialog.component';
import { AskBtnComponent } from './user.app/_askquestion/ask-btn/ask-btn.component';
import { AskDialogComponent } from './user.app/_askquestion/ask-dialog/ask-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    NavigationBarComponent,
    OrderByPipe,
    Error404Component,
    UserRegisterComponent,
    ContactComponent,
    ContactFormComponent,
    ContactInfoComponent,
    LoginComponent,
    SubmitSuccessComponent,
    UserhomeComponent,
    FeedComponent,
    SiteloginComponent,
    AboutUsComponent,
    OnlineCoachingComponent,
    SamplePapersComponent,
    MockTestComponent,
    ProblemCardComponent,
    MiniProfileComponent,
    CardFilterComponent,
    ProblemDialogComponent,
    AskBtnComponent,
    AskDialogComponent
  ],
  entryComponents : [ProblemDialogComponent , AskDialogComponent],
  imports: [
    BrowserModule ,
    BrowserAnimationsModule,
    HttpModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToastModule.forRoot(),
    MatCardModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatInputModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
  ],

  providers: [NavbarService, GlobalService , StaticContentService , UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import {GlobalService} from '../services-global/global.service';
import { ContactInterface } from '../Interfaces/Global.interface';
import { error } from 'util';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  _contactInfoArray: ContactInterface[];
  constructor(private gs: GlobalService) { }

  ngOnInit() {
    this._pageDetails();
  }

  _pageDetails(): any {
    this.gs.getContactPageDetails().subscribe(
      (responseData) => {
        this._contactInfoArray = responseData;
      },
      (err) => {
        console.log(err);
      }
    );
  }

}

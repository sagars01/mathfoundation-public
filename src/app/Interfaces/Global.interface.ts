export interface ContactInterface {
    ct_id: number;
    ct_company_address: string;
    ct_company_name: string;
    ct_page_desc: string;
    ct_company_email: string;
    ct_company_mbnum: number;
}

export interface LoginInterface {
    tokenß: string;
    status: any;
}

export interface Country {
    country: any;
    code: any;
}

export interface RegisterUser {
    registered: any;
}

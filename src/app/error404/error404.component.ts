import { Component, OnInit } from '@angular/core';
import { StaticContentService } from '../services-global/static-content.service';

@Component({
  selector: 'app-error404',
  templateUrl: './error404.component.html',
  styleUrls: ['./error404.component.css']
})
export class Error404Component implements OnInit {
  bgImage: any;
  constructor(private sc: StaticContentService) { }

  ngOnInit() {
    this.bgImage = this.sc.getPageNotFoundImage('/404');
  }

}
